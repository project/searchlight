<?php
/**
 * @file
 * Install, update and uninstall functions for the searchlight module.
 *
 */


/**
 * Implements hook_schema().
 */
function searchlight_schema() {
  $schema = array();
  $schema['searchlight_datasource'] = array(
    'description' => 'Storage for Searchlight datasource configuration.',
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'primary key' => 'oid',
      'object' => 'SearchlightDatasource',
      'identifier' => 'searchlight_datasource',
      'default hook' => 'searchlight_default_datasources',
      'api' => array(
        'owner' => 'searchlight',
        'api' => 'datasource', // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'oid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'name' => array(
        'description' => 'The unique identifier for a datasource.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'views_name' => array(
        'description' => 'Views name.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'views_display' => array(
        'description' => 'Views display.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'base_table' => array(
        'description' => 'The base table for this datasource.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'fields' => array(
        'description' => 'Serialized storage of datasource field settings.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'filters' => array(
        'description' => 'Serialized storage of datasource filter settings.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'options' => array(
        'description' => 'Serialized storage of other datasource options.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('oid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  $schema['searchlight_environment'] = array(
    'description' => 'Storage for Searchlight environment configuration.',
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'primary key' => 'oid',
      'object' => 'SearchlightEnvironment',
      'identifier' => 'searchlight_environment',
      'default hook' => 'searchlight_default_environments',
      'api' => array(
        'owner' => 'searchlight',
        'api' => 'environment', // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'oid' => array(
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'name' => array(
        'description' => 'The unique identifier for an environment.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'views_name' => array(
        'description' => 'Views name.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'views_display' => array(
        'description' => 'Views display.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'view_display' => array(
        'description' => 'The view/display combination for which this environment is used.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'facets' => array(
        'description' => 'Serialized storage of environment facet settings.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'options' => array(
        'description' => 'Serialized storage of other environment options.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('oid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  $schema['searchlight_search'] = array(
    'description' => 'Stores a record of an items status in the index.',
    'fields' => array(
      'type' => array(
        'description' => 'The type of item.',
        'type' => 'varchar',
        'length' => '128',
        'not null' => TRUE,
      ),
      'oid' => array(
        'description' => 'The primary identifier for an item.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the item should be available in the index.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'status' => array('status'),
    ),
    'primary key' => array('type', 'oid'),
  );
  return $schema;
}

/**
 * Implements hook_enable().
 */
function searchlight_enable() {
  searchlight_invalidate_index();
}

/**
 * Implements hook_install().
 */
function searchlight_install() {
}

/**
 * Implements hook_uninstall().
 */
function searchlight_uninstall() {
  variable_del('searchlight_views');
  variable_del('searchlight_backend');
  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query("DELETE FROM {variable} WHERE name LIKE 'searchlight_backend_%'") */
  db_delete('variable')
  ->condition('name', 'searchlight_backend_%', 'LIKE')
  ->execute();
}

/**
 * Update table schema.
 */
function searchlight_update_7000() {
  foreach (array('searchlight_datasource', 'searchlight_environment') as $table) {
    if (!db_field_exists($table, 'oid')) {
      db_drop_primary_key($table);
      db_drop_unique_key($table, 'name');
      db_add_field($table, 'oid', array(
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ), array(
        'primary key' => array('oid'),
        'unique keys' => array(
          'name' => array('name'),
        ),
      ));
    }
    if (!db_field_exists($table, 'views_name')) {
      db_add_field($table, 'views_name', array(
        'description' => 'Views name.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ));
    }
    if (!db_field_exists($table, 'views_display')) {
      db_add_field($table, 'views_display', array(
        'description' => 'Views display.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ));
    }
  }
  drupal_flush_all_caches();
  drupal_get_schema(NULL, TRUE);
}

/**
 * Rebuild datasources & environments.
 */
function searchlight_update_7001(&$sandbox) {
  if (!isset($sandbox['progress'])) {
    $sandbox['datasources'] = db_query('SELECT name FROM {searchlight_datasource}')->fetchCol();
    $sandbox['environments'] = db_query('SELECT name FROM {searchlight_environment}')->fetchCol();
    $sandbox['total'] = count($sandbox['datasources']) + count($sandbox['environments']);
    $sandbox['progress'] = 0;
  }

  if (!empty($sandbox['datasources'])) {
    $name = array_shift($sandbox['datasources']);
    // TODO: the class will be changed.
    $datasource = searchlight_datasource_load($name);
    $datasource->init();
    if (!empty($datasource->view)) {
      $view = views_get_view($datasource->view->name, TRUE);
      if (empty($view)) {
        $datasource->view->save();
        $view = views_get_view($datasource->view->name, TRUE);
      }
      $datasource->views_name = $view->name;
      $datasource->views_display = key($view->display);
      searchlight_datasource_save($datasource);

      $sandbox['progress']++;
      $sandbox['#finished'] = $sandbox['progress'] / $sandbox['total'];
    }
  }

  if (!empty($sandbox['environments'])) {
    $name = array_shift($sandbox['environments']);
    $environment = searchlight_environment_load($name);
    if ($environment->initView()) {
      $environment->save();

      $sandbox['progress']++;
      $sandbox['#finished'] = $sandbox['progress'] / $sandbox['total'];
    }
  }
}
