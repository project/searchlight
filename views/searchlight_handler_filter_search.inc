<?php

class searchlight_handler_filter_search extends views_handler_filter {
  /**
   * Override of pre_query().
   */
  function pre_query() {
    searchlight_views_init_query($this->view);
  }

  /**
   * Override of query().
   */
  function query() {
    if (!empty($this->query->searchlight)) {
      $value = is_array($this->value) ? reset($this->value) : $this->value;
      $value = trim($value);

      // There is a search query.
      if (!empty($value)) {
        $this->query->set_search_buildmode('search');
      }
      // There is no search query.
      else {
        // If we are replacing a core search filter the 'operator' option may be
        // set. Use this to determine buildmode.
        if (!empty($this->options['operator']) && in_array($this->options['operator'], array('required', 'optional'))) {
          if ($this->options['operator'] === 'required') {
            $this->query->set_search_buildmode('empty');
          }
        }
        // Check the hideEmpty option and check whether to blank the resultset.
        elseif ($this->options['hideEmpty']) {
          $this->query->set_search_buildmode('empty');
        }
      }

      $this->query->set_search_options($this->options);
      $this->query->set_search_query($value);

      // In case we have an environment.
      $environment = searchlight_environment_instance(NULL, $this->view->name, $this->view->current_display);
      if ($environment && $environment->initView($this->view)) {
        $environment->query($this->query);
      }
    }
  }

  /**
   * Override of option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    if ($backend = searchlight_get_backend()) {
      $options += $backend->viewsOptionDefinition($this);
    }
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($backend = searchlight_get_backend()) {
      $backend->viewsOptionsForm($form, $form_state, $this);
    }
  }

  /**
   * Provide a textfield for search query.
   */
  function value_form(&$form, &$form_state) {
    if ($backend = searchlight_get_backend()) {
      $backend->viewsValueForm($form, $form_state, $this);
    }

    // In case we have an environment.
    // TODO: move
    $environment = searchlight_environment_instance(NULL, $this->view->name, $this->view->current_display);
    if ($environment && $environment->initView($this->view)) {
      if ($adapter = facetapi_adapter_load('searchlight@' . $environment->name)) {
        $items = $adapter->getAllActiveItems();
        if (!empty($items)) {
          // Add active filters to form values.
          $filter_key = variable_get('searchlight_facet_key', 'sl');
          foreach ($items as $k => $v) {
            $form["{$filter_key}[{$v['pos']}]"] = array(
              '#type' => 'hidden',
              '#value' => $k,
            );
          }
        }
      }
    }
  }
}
