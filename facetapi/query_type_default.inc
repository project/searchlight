<?php

/**
 * @file
 */

/**
 * Default query types.
 */
class SearchlightQueryTypeDefault extends FacetapiQueryType implements FacetapiQueryTypeInterface {

  // TODO
  var $value;

  public $name;
  public $field;
  public $options;
  public $environment;

  // TODO: save with field.
  public $views_name;
  public $views_display;

  // Render infrastructure objects.
  public $view;
  public $handler;

  /**
   * Constructor.
   */
  public function __construct(FacetapiAdapter $adapter, array $facet) {
    parent::__construct($adapter, $facet);

    $this->initFacet();
  }

  /**
   * Returns the query type associated with the plugin.
   */
  static public function getType() {
    return 'default';
  }

  /**
   * Adds the filter to the query object.
   */
  public function execute($query) {
    $active = $this->adapter->getActiveItems($this->facet);

    // Filter the query if there is an active facet value.
    // Remove any existing filters for this same value in order to cleanly
    // "replace" the filter condition. Note that this requires the Searchlight
    // facets filter handler to be placed *after* any other Views filters that
    // should be replaced when a facet is active.
    if (!empty($active)) {
      // Enable search when there's active items.
      $query->set_search_buildmode('search', TRUE);

      foreach ($query->search_filter as $key => $filter) {
        if ($filter['field'] === $this->name) {
          unset($query->search_filter[$key]);
        }
      }

      foreach ($active as $value => $item) {
        $query->search_filter[] = array(
          'field' => $this->name,
          'operator' => '=',
          'args' => array($value),
        );
      }
    }

    // Always add the search facet since it's enabled.
    // @see class SearchlightEnvironment -> function query().
    $settings = $this->getSettings();
    $limit = isset($settings->settings['hard_limit']) ? $settings->settings['hard_limit'] : 0;
    $query->add_search_facet($this->name, $limit);
  }

  /**
   * Initializes the facet's render array.
   */
  public function build() {
    $build = array();

    if (empty($this->environment->query)) {
      return $build;
    }

    $result = $this->environment->query->get_search_facet($this->facet['name']);
    foreach ($result as $record) {
      $build[$record['id']] = array('#count' => $record['count']);
    }

    return $this->buildMarkups($build);
  }

  /**
   * Build markups.
   */
  private function buildMarkups($build) {
    if (!empty($this->options['override_markup'])) {
      return $this->formatMarkups($build);
    }

    $ids = array_keys($build);
    if (empty($ids)) {
      return $build;
    }

    // Build markups with views.
    if (!$this->buildView()) {
      return $build;
    }

    $rows = array();
    switch ($this->field['usage']) {
      case 'attribute':
        // Build rows.
        foreach ($ids as $id) {
          $row = new stdClass();
          $row->{$this->name} = $id;
          $rows[$id] = $row;
        }
        break;

      case 'multivalue':
        $query = &$this->view->query;
        $real_field = $field_alias = NULL;

        // Add ids to views query.
        foreach ($query->fields as $alias => $query_field) {
          if ($query_field['field'] === $this->field['field'] && $query_field['table'] === $this->field['table_alias']) {
            $real_field = $query_field['table'] . '.' . $query_field['field'];
            $field_alias = $query_field['alias'];
            $query->add_where(0, $real_field, $ids, 'IN');
            break;
          }
        }

        if (!empty($real_field)) {
          // Add groupby to DB query - not to views query because...
          $query = $query->query();
          $query->groupBy($real_field);

          // Execute and build rows.
          $result = $query->execute();
          foreach ($result as $row) {
            $rows[$row->$field_alias] = $row;
          }
        }
        break;
    }

    // Render item titles.
    if (!empty($rows)) {
      $this->handler->pre_render($rows);
      foreach ($rows as $id => $row) {
        if (!empty($build[$id])) {
          $build[$id]['#markup'] = strip_tags($this->handler->render($row));
        }
      }
    }

    $this->destroyView();

    return $build;
  }

  /**
   * Simply format the markups.
   */
  public function formatMarkups($build) {
    return $build;
  }

  /**
   * Init this facet.
   */
  public function initFacet() {
    $this->field = $this->facet['datasource_field'];
    $this->name = $this->field['name'];
    $this->environment = $this->adapter->getEnvironment();

    $settings = $this->getSettings();
    $options = !empty($settings->settings['searchlight']) ? $settings->settings['searchlight'] : array();
    $this->options = $options + $this->optionsDefault();

    // TODO: save these with the field.
    if ($this->field['usage'] === 'multivalue' && !empty($this->field['view'])) {
      $identifier = $this->field['view'];
      $split = explode(':', $identifier);
      if (count($split) === 2 && $view = views_get_view($split[0])) {
        $this->views_name = $split[0];
        $this->views_display = $split[1];
      }
    }
  }

  /**
   * Build view, get field handler.
   */
  private function buildView() {
    $this->view = $this->handler = NULL;

    switch ($this->field['usage']) {
      case 'attribute':
        $this->view = $this->environment->datasource->view->copy();
        if (!empty($this->view) && $this->view->build()) {
          foreach ($this->view->field as $field_handler) {
            if ($field_handler->field_alias === $this->name) {
              $this->handler = $field_handler;
              break;
            }
          }
        }
        break;

      case 'multivalue':
        if (!empty($this->views_name) && !empty($this->views_display)) {
          $this->view = views_get_view($this->views_name);
          if (!empty($this->view) && $this->view->build($this->views_display)) {
            foreach ($this->view->field as $field_handler) {
              if (in_array($field_handler->field_alias, array($this->field['name'], $this->field['label_field']['name']), TRUE)) {
                $this->handler = $field_handler;
                break;
              }
            }
          }
        }
        break;
    }

    return !empty($this->handler);
  }

  /**
   * Destroy view.
   *
   * TODO: cleanup?
   */
  private function destroyView() {
    $this->view->destroy();
  }

  /**
   * Provide default values for options.
   */
  function optionsDefault() {
    return array(
      'override_markup' => FALSE,
    );
  }

  /**
   * Provide an options form to be exposed in the Environment editor.
   */
  function optionsForm(&$form, &$form_state) {
  }

}
