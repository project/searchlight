<?php

/**
 * @file
 */

/**
 * Facet API adapter.
 */
class SearchlightAdapter extends FacetapiAdapter {

  /**
   * .
   */
  protected $environment;

  /**
   * The total number of results in the query.
   */
  protected $resultCount;

  /**
   * Returns a boolean flagging whether $this->info['searcher'] executed a search.
   */
  public function searchExecuted() {
    return (isset($this->keys) || !empty($this->activeItems['filter']));
  }

  /**
   * Returns a boolean flagging whether facets in a realm shoud be displayed.
   */
  public function suppressOutput($realm_name) {
    // TODO
  }

  /**
   * Processes a raw array of active filters.
   */
  public function setParams(array $params = array(), $filter_key = '') {
    $filter_key = variable_get('searchlight_facet_key', 'sl');
    return parent::setParams($params, $filter_key);
  }

  /**
   * Returns the path to the admin settings for a given realm.
   */
  public function getPath($realm_name) {
    return 'admin/config/search/settings/environment/' . $this->info['environment_name'] . '/facets/' . $realm_name;
  }

  /**
   * Returns the search path.
   */
  public function getSearchPath() {
    if (!empty($this->environment)) {
      return $this->environment->getURLPath();
    }
  }

  /**
   * Sets the result count.
   */
  public function setResultCount($count) {
    $this->resultCount = $count;
    return $this;
  }

  /**
   * Returns the number of results returned by the search query.
   */
  public function getResultCount() {
    return $this->resultCount;
  }

  /**
   * Allows for backend specific overrides to the settings form.
   */
  public function settingsForm(&$form, &$form_state) {
    extract($form['#facetapi']);

    // Allow facets to have their global settings.
    $form['searchlight'] = array(
      '#parents' => array('global', 'searchlight'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Searchlight settings'),
      '#description' => t('The configuration options below apply to this facet across <em>all</em> realms.'),
    );
    $facet_query = $this->getFacetQuery($facet);
    if ($facet_query instanceof SearchlightFacetapiQueryDefault) {
      $facet_query->optionsForm($form['searchlight'], $form_state);
    }
  }

  /**
   * Provides default values for the backend specific settings.
   */
  public function getDefaultSettings() {
    return array();
  }

  /**
   * Returns the human readable value associated with a facet's raw value.
   */
  public function getMappedValue($facet_name, $value) {
    if (!empty($this->processors[$facet_name])) {
      $build = $this->processors[$facet_name]->getBuild();
      if (!empty($build[$value])) {
        return $build[$value];
      }
    }

    return parent::getMappedValue($facet_name, $value);
  }

  /**
   * Initializes facet builds, adds breadcrumb trail.
   */
  protected function processFacets() {
    if (!$this->processed) {
      // Get a (?) environment.
      $this->environment = $this->getEnvironment();
    }

    return parent::processFacets();
  }

  /**
   * .
   */

  /**
   * .
   */
  public function getEnvironmentName() {
    return $this->info['environment_name'];
  }

  /**
   * .
   */
  public function getEnvironment() {
    return searchlight_environment_instance($this->info['environment_name']);
  }

}
