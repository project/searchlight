<?php

/**
 * @file
 */

/**
 * Extension of FacetapiUrlProcessor.
 *
 * @see class FacetapiUrlProcessorStandard().
 */
class SearchlightFacetapiUrlProcessor extends FacetapiUrlProcessor {

  /**
   * Implements FacetapiUrlProcessor::fetchParams().
   *
   * Pulls facet params from the $_GET variable.
   */
  public function fetchParams() {
    return $_GET;
  }

  /**
   * Implements FacetapiUrlProcessor::normalizeParams().
   *
   * Strips the "q" and "page" variables from the params array.
   */
  public function normalizeParams(array $params, $filter_key = '') {
    return drupal_get_query_parameters($params, array('q', 'page'));
  }

  /**
   * Implements FacetapiUrlProcessor::getQueryString().
   */
  public function getQueryString(array $facet, array $values, $active) {
    $qstring = $this->params;
    $active_items = $this->adapter->getActiveItems($facet);

    // Appends to qstring if inactive, removes if active.
    foreach ($values as $value) {
      if ($active && isset($active_items[$value])) {
        unset($qstring[$this->filterKey][$active_items[$value]['pos']]);
      }
      elseif (!$active) {
        $field_alias = rawurlencode($facet['field alias']);
        $qstring[$this->filterKey][] = $field_alias . ':' . $value;
      }
    }

    // Resets array keys, returns query string.
    $qstring[$this->filterKey] = array_values($qstring[$this->filterKey]);
    return array_filter($qstring);
  }

  /**
   * Implements FacetapiUrlProcessor::setBreadcrumb().
   */
  public function setBreadcrumb() {
  }

}
