<?php

/**
 * Abstract class, defines interface for configurable objects that use views.
 */
class SearchlightViews extends views_object {
  // The table: used by DB queries and CTools, defined in schema.
  public $table;

  // Storage variables: saved to DB or code when an environment is saved.
  public $name;
  public $views_name;
  public $views_display;

  // Legacy support.
  public $view_display = '';

  // Class objects.
  public $plugins = array();

  /**
   * Options default.
   */
  function option_definition() {
    return parent::option_definition();
  }

  /**
   * Constructor.
   */
  function construct() {
    return parent::construct();
  }

  /**
   * Options form.
   */
  function optionsForm(&$form, &$form_state) {
    views_include('admin');
    views_include('form');
  }

  /**
   * Submit handler for options form.
   */
  function optionsSubmit($form, &$form_state) {
  }

  /**
   * Database CRUD methods ====================================================
   *
   * TODO: consider CTools export CRUD functions.
   */

  /**
   * Inserts or updates this object into the database.
   *
   * @param $editing
   *   Boolean flag for whether this object is still being edited. Allows for
   *   quick saves without invalidating repeatedly.
   *
   * @return
   *   Returns true on success, false on failure.
   */
  public function save($editing = FALSE) {
    if (empty($this->table) || empty($this->name)) {
      return FALSE;
    }
    ctools_include('export');
    $existing = searchlight_object_load($this->table, $this->name, TRUE);
    if ($existing && ($existing->export_type & EXPORT_IN_DATABASE)) {
      $return = drupal_write_record($this->table, $this, 'name');
    }
    else {
      $return = drupal_write_record($this->table, $this);
    }
    searchlight_object_load($this->table, NULL, TRUE);
    if (!$editing) {
      searchlight_invalidate_cache();
    }
    return $return;
  }

  /**
   * Deletes this object from the database.
   *
   * @return
   *   Returns true on success, false on failure.
   */
  public function delete() {
    if (empty($this->table) || empty($this->name)) {
      return FALSE;
    }
    db_delete($this->table)
      ->condition('name', $this->name)
      ->execute();
    searchlight_invalidate_cache();
    return TRUE;
  }

  /**
   * Views integration methods ================================================
   */

  /**
   * Initialize the view and all plugins.
   */
  public function initView($view = NULL, $reset = FALSE) {
    if (!isset($this->view) || $reset) {
      $this->view = NULL;

      // Build the view if none was explicitly provided.
      $view = isset($view) ? $view : $this->buildView();

      // Create a reference to the view and init plugins.
      if ($view) {
        $this->view = $view;
        $this->plugins = array();
      }
    }
    return !empty($this->view);
  }

  /**
   * .
   */
  protected function buildView() {
    if (!empty($this->views_name) && !empty($this->views_display)) {
      if ($view = views_get_view($this->views_name)) {
        if ($view->build($this->views_display)) {
          return $view;
        }
      }
    }

    // Legacy support.
    elseif (!empty($this->view_display)) {
      $split = explode(':', $this->view_display);
      if (count($split) === 2 && $view = views_get_view($split[0])) {
        $this->views_name = $split[0];
        $this->views_display = $split[1];
        if ($view->build($this->views_display)) {
          return $view;
        }
      }
    }
  }
}
